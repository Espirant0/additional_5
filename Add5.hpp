#pragma once
#define N 10000

namespace saw {
	void Input(int& n, int mas[N]);
	void DeleteAndDuplicate(int& n, int mas[N], int& s, int& s1, int& sum, int& umn, int& data);
	void Output(int& n, int mas[N]);
}